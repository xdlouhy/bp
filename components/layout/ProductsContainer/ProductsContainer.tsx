import React, { FC } from "react";
import { Pagination } from "../../product/Pagination/Pagination";

interface ProductsContainerProps {
  children: React.ReactNode;
}
export const ProductsContainer: FC<ProductsContainerProps> = ({ children }) => {
  return (
    <>
      <div className="min-h-[85vh]">
        <div className="container grid justify-between py-4 md:grid-cols-5 sm:grid-cols-2">
          {children}
        </div>
      </div>
      <Pagination />
    </>
  );
};
