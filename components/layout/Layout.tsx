import React, { FC } from "react";
import Navbar from "./navbar/Navbar";
import { Footer } from "./Footer/Footer";
import { useQuery } from "react-query";
import { fetchCategories } from "../../actions/categories";
import { categories } from "../../signals";

export const links = [
  { display: "Home", to: "/" },
  { display: "About", to: "/about" },
  { display: "Cart", to: "/cart" },
];

export const Layout: FC<{ children: React.ReactNode }> = ({ children }) => {
  const { data: categoriesData } = useQuery({
    queryKey: ["categories"],
    queryFn: () => fetchCategories(),
  });

  categories.value = categoriesData;

  return (
    <div className="min-h-screen">
      <Navbar links={links} />
      <div className="container min-h-screen mx-auto">{children}</div>
      <Footer />
    </div>
  );
};
