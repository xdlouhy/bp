import { FC, HTMLProps } from "react";

export const PrimaryButton: FC<
  { preventDefault?: boolean } & React.ButtonHTMLAttributes<HTMLButtonElement>
> = ({ preventDefault, className, children, onClick, ...rest }) => {
  return (
    <button
      className={
        "py-2 px-4 m-2 rounded-full bg-red-400 text-white hover:bg-red-200 " +
        className
      }
      onClick={(e) => {
        preventDefault && e.preventDefault();
        if (onClick) {
          onClick(e);
        }
      }}
      {...rest}
    >
      {children}
    </button>
  );
};
