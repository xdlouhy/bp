import { FC } from "react";
import { Search } from "../../froms/search/Search";
import { NavbarTile } from "./NavbarTile/NavbarTile";
import { signIn, signOut, useSession } from "next-auth/react";
import { getDisplayFirstName, isAdmin } from "../../product/utils";
import Link from "next/link";

export type NavbarProps = {
  links: {
    display: string;
    to: string;
  }[];
};

const Navbar: FC<NavbarProps> = ({ links }) => {
  const session = useSession();
  const isSignedIn = session.status === "authenticated";

  return (
    <nav className="flex items-center justify-between h-20 p-6 text-white bg-red-400 ">
      <div className="flex flex-row w-1/2 ">
        <Link href={"/"}>
          <div className="pt-2 text-xl font-medium">Testshop</div>
        </Link>
        <Search />
      </div>
      <div className="flex items-center">
        {links.map((link) => (
          <NavbarTile key={link.to} display={link.display} link={link.to} />
        ))}
        {isAdmin(session.data) &&
          NavbarTile({ display: "Create product", link: "/products/create" })}
        <div
          className="px-3 py-2 mx-2 text-white bg-red-300 rounded-full hover:bg-secondary-default hover:bg-red-200 hover:cursor-pointer"
          onClick={() => (isSignedIn ? signOut() : signIn())}
        >
          {"Sign " + (isSignedIn ? "out" : "in")}
        </div>
        {isSignedIn ? (
          `Welcome, ${getDisplayFirstName(session?.data?.user.name as string)}!`
        ) : (
          <NavbarTile display="Register" link="/auth/register"></NavbarTile>
        )}
      </div>
    </nav>
  );
};

export default Navbar;
