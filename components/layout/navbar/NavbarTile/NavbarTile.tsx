import Link from "next/link";
import React, { FC, ReactNode } from "react";

interface NavbarTileProps {
  display: string;
  link: string;
}

export const NavbarTile: FC<NavbarTileProps> = ({ display, link }) => {
  return (
    <Link href={link}>
      <div className="px-3 py-2 mx-2 text-white bg-red-300 rounded-full whitespace-nowrap hover:bg-secondary-default hover:bg-red-200">
        {display}
      </div>
    </Link>
  );
};
