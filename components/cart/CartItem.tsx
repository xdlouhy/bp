import { XCircleIcon } from "@heroicons/react/24/solid";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";
import { useCart } from "../../actions/cart";
import { ShoppingCartItem } from "../../config/types";
import { getItemTotal, getProductMainPhoto } from "../product/utils";

export const CartItem: FC<{ item: ShoppingCartItem; index: number }> = ({
  item,
  index,
}) => {
  const { dollars, cents } = getItemTotal(item);
  const { updateCart, removeFromCart } = useCart();
  return (
    <div className="flex flex-row py-4 rounded-md shadow-md">
      <Link href={`/products/${item.product.id}`}>
        <Image
          width={200}
          height={200}
          className="object-contain w-40 h-32"
          alt={item.product.name}
          src={getProductMainPhoto(item.product)}
        />
      </Link>
      <div className="flex flex-col w-full px-4">
        <Link href={`/products/${item.product.id}`}>
          <h2 className="text-lg">{item.product.name}</h2>
        </Link>
        <div>{item.product.description}</div>
      </div>
      <div className="flex flex-col justify-center px-4">
        <input
          type="number"
          className="w-16 h-8 px-4 py-2 rounded-md focus:outline-none focus:ring-2 focus:ring-black focus:border-transparent focus:ring-opacity-50"
          min={1}
          defaultValue={item.quantity}
          onChange={(e) => {
            updateCart(index, +e.target.value);
          }}
        />
      </div>
      <div className="flex flex-col justify-center px-4">
        ${item.product.priceDollars}.{item.product.priceCents}/unit
      </div>
      <div className="flex flex-col justify-center px-4 font-bold">
        ${dollars}.{cents}
      </div>
      <div className="flex flex-col justify-center px-4 font-bold">
        <XCircleIcon
          className="w-6 h-6 text-gray-300 hover:cursor-pointer"
          onClick={() => removeFromCart(index)}
        />
      </div>
    </div>
  );
};
