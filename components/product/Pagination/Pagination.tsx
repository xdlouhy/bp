import React from "react";
import { PaginationItem } from "./PaginationItem";
import { searchFilters } from "../../../signals";

const previous = () => {
  const page = searchFilters.value.page;
  const newValue = page && page > 1 ? page - 1 : 1;
  searchFilters.value = { ...searchFilters.value, page: newValue };
};

const next = () => {
  const page = searchFilters.value.page;
  const newValue = page && page < 5 ? page + 1 : 5;
  searchFilters.value = { ...searchFilters.value, page: newValue };
};

const set = (page: number) => {
  searchFilters.value = { ...searchFilters.value, page: page };
};

export const Pagination = () => {
  return (
    <div
      aria-label="Page navigation"
      className="flex justify-center w-full py-4"
    >
      <div className="flex flex-row text-white bg-red-400 border border-gray-200 rounded-lg shadow-sm cursor-pointer">
        <PaginationItem onClick={previous}>Previous</PaginationItem>
        {[1, 2, 3, 4, 5].map((n) => (
          <PaginationItem value={n} onClick={() => set(n)}>
            {n}
          </PaginationItem>
        ))}
        <PaginationItem onClick={next}>Next</PaginationItem>
      </div>
    </div>
  );
};
