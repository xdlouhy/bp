import React, { FC } from "react";
import { searchFilters } from "../../../signals";

export const PaginationItem: FC<
  { value?: number } & React.HTMLAttributes<HTMLButtonElement>
> = ({ children, value, ...rest }) => {
  const page = searchFilters.value.page;
  return (
    <button
      className={
        "px-3 py-2 ml-0 leading-tight border border-red-200 hover:bg-red-300" +
        (page && page === value ? " bg-red-300" : "")
      }
      {...rest}
    >
      {children}
    </button>
  );
};
