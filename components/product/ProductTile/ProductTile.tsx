import Image from "next/image";
import Link from "next/link";
import { FC } from "react";
import { ProductWithCategories } from "../../../config/types";
import { getProductMainPhoto } from "../utils";

export const ProductTile: FC<{ product: ProductWithCategories }> = ({
  product,
}) => {
  return (
    <Link href={`products/${product.id}`}>
      <div className="flex flex-col items-center m-4 border rounded-md shadow-md">
        <div className="overflow-hidden ">
          <Image
            width={200}
            height={200}
            className="object-contain h-44"
            src={getProductMainPhoto(product)}
            alt={product.name}
          ></Image>
        </div>
        <div className="flex flex-row justify-around w-full overflow-hidden whitespace-nowrap">
          <span>{product.name}</span>
          <span>
            ${product.priceDollars}.{product.priceCents}
          </span>
        </div>
      </div>
    </Link>
  );
};
