import { Product } from "@prisma/client";
import React, { FC } from "react";
import { getProductMainPhoto } from "../utils";
import { ProductData } from "../../../config/types";

export const ProductMainImage: FC<{ product: ProductData }> = ({ product }) => {
  return (
    <div className="max-w-xl overflow-hidden rounded-lg">
      {/* Main image */}
      <img
        className="object-cover w-full h-full max-w-full"
        src={getProductMainPhoto(product)}
        alt={product.name}
      />
    </div>
  );
};
