import { Product } from "@prisma/client";
import {
  ProductData,
  ShoppingCart,
  ShoppingCartItem,
} from "../../config/types";
import { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/react";
import { Session } from "next-auth";

export const getProductMainPhoto = (product: Product) => {
  if (!product.photo) {
    return "/notfound.jpg";
  }
  return product.photo;
};

export const getItemTotal = (item: ShoppingCartItem) => {
  const overflow = ((item.product.priceCents * item.quantity) / 100) >> 0;
  const cents = (item.product.priceCents * item.quantity) % 100;
  const dollars = item.product.priceDollars * item.quantity + overflow;

  return { dollars, cents };
};

export const getCartTotal = (cart: ShoppingCart) => {
  const total = cart.reduce((acc, item) => acc + getItemTotal(item).dollars, 0);
  const totalCents = cart.reduce(
    (acc, item) => acc + getItemTotal(item).cents,
    0
  );
  return {
    total: total + ((totalCents / 100) >> 0),
    totalCents: totalCents % 100,
  };
};

// https://stackoverflow.com/questions/62116454/how-to-type-define-a-zip-function-in-typescript
export const zip = <T extends unknown[][]>(
  ...args: T
): { [K in keyof T]: T[K] extends (infer V)[] ? V : never }[] => {
  const minLength = Math.min(...args.map((arr) => arr.length));
  // @ts-expect-error This is too much for ts
  return range(minLength).map((i) => args.map((arr) => arr[i]));
};

export const okOrNotFound = async (
  promise: Promise<any>,
  res: NextApiResponse
) => {
  return promise
    .then((data) => res.status(200).json(data))
    .catch((err) => {
      res.status(404).json({ message: JSON.stringify(err) });
    });
};

export const unauthorized = (res: NextApiResponse) => {
  return res.status(401).json({ message: "Unauthorized" });
};

export const forbidden = (res: NextApiResponse) => {
  return res.status(403).json({ message: "Forbidden" });
};

export const methodNotAllowed = (res: NextApiResponse) => {
  return res.status(405).json({ message: "Method not allowed" });
};

export const getDisplayFirstName = (name: string) => {
  const [firstName] = name.split(" ");
  return firstName;
};

export const isSuperuser = async (req: NextApiRequest) => {
  return await getSession({ req }).then(
    (session) => session?.user.isSuperuser as boolean
  );
};

export const getSenderId = async (req: NextApiRequest) => {
  return await getSession({ req })
    .then((session) => session?.user.id as number)
    .catch(() => null);
};

export const isAllowed = async (req: NextApiRequest, userId: number) => {
  const isSuper = await isSuperuser(req);
  const senderId = await getSenderId(req);
  return isSuper || senderId === userId;
};

export const getResource = async <T extends { userId: number | null }>(
  prismaResource: any,
  id: number
): Promise<T> => {
  return await prismaResource.findUnique({
    where: {
      id: id,
    },
  });
};

export const getResourceOwner = async (prismaResource: any, id: number) => {
  return Number(
    getResource(id, prismaResource).then((resource) => resource?.userId)
  );
};

export const isAdmin = (session: Session | null) => {
  return session && (session?.user.isSuperuser as boolean);
};

export const isInCart = (cart: ShoppingCart, productId: number) => {
  return cart.some((item) => item.product.id === productId);
};
