import { Signal, signal } from "@preact/signals-react";
import { Category } from "@prisma/client";
import React, { FC } from "react";
import { categories } from "../../../signals";

export type CategorySelectionProps = {
  selected?: number[];
} & React.HTMLAttributes<HTMLDivElement>;

export const formSelectedCategoryIds: Signal<number[]> = signal([]);

export const CategorySelection: FC<CategorySelectionProps> = ({
  selected,
  ...rest
}) => {
  const setCategory = (checked: boolean, categoryId: number) => {
    if (checked) {
      formSelectedCategoryIds.value.push(categoryId);
    } else {
      formSelectedCategoryIds.value = formSelectedCategoryIds.value.filter(
        (element) => element !== categoryId
      );
    }
    console.log(formSelectedCategoryIds.value);
  };

  return (
    <div className="grid grid-cols-4" {...rest}>
      {" "}
      {categories.value?.map((category) => (
        <div key={`category-${category.id}`}>
          <input
            key={category.name}
            type="checkbox"
            defaultChecked={selected?.includes(category.id)}
            onChange={(e) => {
              setCategory(e.target.checked, category.id);
            }}
          />
          <label htmlFor={category.name}> {category.name}</label>
        </div>
      ))}
    </div>
  );
};

export default CategorySelection;
