import React, { FC } from "react";
import { Form } from "../Form";
import { Review } from "@prisma/client";

export const ReviewForm: FC<{ productId: number; review?: Review }> = ({
  productId,
  review,
}) => {
  return (
    <Form heading="Create review">
      <input type="hidden" name="productId" defaultValue={productId} />
      <input type="hidden" name="id" defaultValue={review?.id} />
      <input type="hidden" name="userId" defaultValue={review?.authorId} />
      <input type="hidden" name="rating" defaultValue={review?.rating} />
      <input type="text" name="text" defaultValue={review?.text} />
    </Form>
  );
};
