import React from "react";
import { FieldValues, UseFormRegister, RegisterOptions } from "react-hook-form";

export type TextInputProps<T extends FieldValues> = {
  register: UseFormRegister<T>;
  options?: RegisterOptions<T> | undefined;
  id: keyof T;
} & React.InputHTMLAttributes<HTMLInputElement>;

export const TextInputStyle =
  "bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-red-500 focus:border-red-500 block p-2.5 ";

export const TextInput = <T extends FieldValues>({
  id,
  className,
  register,
  options,
  ...rest
}: TextInputProps<T>) => {
  return (
    <input
      className={TextInputStyle + className}
      // @ts-ignore
      {...register(id, options)}
      {...rest}
    />
  );
};
