import React from "react";
import CategorySelection from "../category/CategorySelection";
import { searchFilters } from "../../../signals";
import { SortingType } from "../../../config/types";
import { Prisma } from "@prisma/client";
import { CategoryFilter } from "./CategoryFilter";
import { FormLabel } from "../TextInputLabel";

export const SearchFiltersForm = () => {
  return (
    <div className="flex flex-row justify-between p-4">
      <div className="flex flex-row gap-4 align-middle">
        <FormLabel className="leading-8">Categories:</FormLabel>
        <div className="px-4 border rounded-md shadow-md border-gray-50">
          <CategoryFilter />
        </div>
      </div>
      <div className="flex flex-row gap-4">
        <div>
          <FormLabel htmlFor="sortby">Sort by:</FormLabel>
          <select
            id="sortby"
            onChange={(e) =>
              (searchFilters.value = {
                ...searchFilters.value,
                sortBy: e.target.value as SortingType,
              })
            }
          >
            <option value={"name"}>Name</option>
            <option value={"price"}>Price</option>
            <option value={"reviews"}>Reviews</option>
          </select>
        </div>
        <div>
          <FormLabel htmlFor="order">Order:</FormLabel>
          <select
            id="order"
            onChange={(e) =>
              (searchFilters.value = {
                ...searchFilters.value,
                sortingDirection: e.target.value as Prisma.SortOrder,
              })
            }
          >
            <option value={"asc"}>Ascending</option>
            <option value={"desc"}>Descending</option>
          </select>
        </div>
      </div>
    </div>
  );
};
