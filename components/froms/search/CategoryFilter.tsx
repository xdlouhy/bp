import React from "react";
import { categories, searchFilters } from "../../../signals";

export const CategoryFilter = () => {
  return (
    <div className="flex flex-row gap-4">
      {" "}
      {categories.value?.map((category) => (
        <div key={`category-filter-${category.id}`}>
          <input
            key={category.name}
            className="cursor-pointer"
            type="checkbox"
            id={"checkbox-" + category.name}
            onChange={(e) => {
              searchFilters.value = {
                ...searchFilters.value,
                categories: e.target.checked
                  ? [...searchFilters.value.categories, category.id]
                  : searchFilters.value.categories.filter(
                      (element) => element !== category.id
                    ),
              };
            }}
          />
          <label
            className="cursor-pointer"
            htmlFor={"checkbox-" + category.name}
          >
            {" "}
            {category.name}
          </label>
        </div>
      ))}
    </div>
  );
};
