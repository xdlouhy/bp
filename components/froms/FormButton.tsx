import React, { FC } from "react";

export const FormButton: FC<React.ButtonHTMLAttributes<HTMLButtonElement>> = ({
  children,
  className,
  ...rest
}) => {
  return (
    <button
      className={
        "text-white bg-red-400 hover:bg-red-200 rounded-lg text-sm px-3 py-1.5 ml-auto inline-flex items-center mt-2 mr-4 " +
        className
      }
      {...rest}
    >
      {children}
    </button>
  );
};

export default FormButton;
