import { useRouter } from "next/router";
import { FC } from "react";
import { useForm } from "react-hook-form";
import { useQueryClient } from "react-query";
import { createProduct, updateProduct } from "../../../actions/products";
import { ProductData, ProductWithCategories } from "../../../config/types";
import { Form } from "../Form";
import { TextInput } from "../TextInput";
import { FormLabel } from "../TextInputLabel";
import CategorySelection, {
  formSelectedCategoryIds,
} from "../category/CategorySelection";
import { LabeledTextInput } from "../LabeledTextInput";

export const ProductForm: FC<{ product?: ProductWithCategories }> = ({
  product,
}) => {
  const queryClient = useQueryClient();
  const router = useRouter();
  const { register, handleSubmit } = useForm({ defaultValues: product });
  return (
    <Form
      heading={(product ? "Edit" : "Create") + " product"}
      onSubmit={handleSubmit((data: ProductWithCategories) => {
        const productData: ProductData = {
          ...data,
          categoryIds: formSelectedCategoryIds.value,
        };

        product ? updateProduct(productData) : createProduct(productData);
        queryClient.invalidateQueries("products");
        router.push("/");
      })}
    >
      <FormLabel htmlFor="name">Product name</FormLabel>
      <TextInput
        className="w-full"
        id="name"
        register={register}
        defaultValue={product?.name}
        required
      />
      <FormLabel htmlFor="priceDollars">Price</FormLabel>
      <div className="flex flex-row">
        <span className="text-4xl">$</span>

        <TextInput
          inputMode="decimal"
          type="number"
          min={0}
          register={register}
          options={{ valueAsNumber: true }}
          id="priceDollars"
          className="w-20 ml-2"
          defaultValue={product?.priceDollars}
          required
        />
        <span className="text-4xl">.</span>
        <TextInput
          inputMode="decimal"
          register={register}
          options={{ valueAsNumber: true }}
          id="priceCents"
          type="number"
          className="w-16"
          defaultValue={product?.priceCents}
          max={99}
          min={0}
          required
        />
      </div>
      <FormLabel htmlFor="description">Product description</FormLabel>
      <textarea
        {...register("description")}
        id="description"
        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg `focus:ring-red-500
          focus:border-red-500 block w-full p-2.5"
        defaultValue={product?.description as string}
        required
      />
      <FormLabel htmlFor="categories">Categories</FormLabel>
      <CategorySelection
        selected={product?.categories.map((category) => category.id)}
      />
      <FormLabel htmlFor="inStock">In stock</FormLabel>
      <div>
        <div className="flex flex-row gap-4 my-2 align-middle justify-items-center">
          <TextInput
            inputMode="decimal"
            type="number"
            register={register}
            options={{ valueAsNumber: true }}
            id="inStock"
            className="w-20"
            defaultValue={product?.inStock}
            required
          />
          <div className="flex items-center gap-1">
            <input
              defaultChecked={product?.isForSale}
              type="checkbox"
              {...register("isForSale")}
            />
            <label className="text-center justify" htmlFor="isForSale">
              {" "}
              This product is available for sale
            </label>
          </div>
        </div>
      </div>
      <LabeledTextInput
        label="Image URL"
        inputMode="decimal"
        register={register}
        id="photo"
        className="w-full"
        defaultValue={product?.photo as string}
      />
    </Form>
  );
};
