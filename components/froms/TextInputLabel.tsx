import React, { FC } from "react";

export const FormLabel: FC<React.LabelHTMLAttributes<HTMLLabelElement>> = ({
  children,
  className,
  ...rest
}) => {
  return (
    <label
      className={"text-sm font-medium text-gray-900 " + className}
      {...rest}
    >
      {children}
    </label>
  );
};
