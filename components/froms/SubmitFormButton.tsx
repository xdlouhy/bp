import React, { FC } from "react";
import FormButton from "./FormButton";

export const SubmitFormButton: FC<
  React.ButtonHTMLAttributes<HTMLButtonElement>
> = ({ children, ...rest }) => {
  return (
    <FormButton type="submit" {...rest}>
      {children}
    </FormButton>
  );
};
