import { useForm } from "react-hook-form";
import { createUser } from "../../../actions/users";
import { UserPostData } from "../../../config/types";
import { Form } from "../Form";
import { LabeledTextInput } from "../LabeledTextInput";

export const RegisterForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<UserPostData>();

  return (
    <Form heading="Create new user" onSubmit={handleSubmit(createUser)}>
      <LabeledTextInput id="email" label="Email" required register={register} />
      <LabeledTextInput
        id="password"
        label="Password"
        register={register}
        type="password"
        minLength={8}
        maxLength={32}
        required
      />
      <LabeledTextInput
        id="name"
        label="Name"
        register={register}
        className="w-full"
      />
      <LabeledTextInput id="phone" label="Phone" required register={register} />
    </Form>
  );
};
