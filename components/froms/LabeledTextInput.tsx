import React, { FC } from "react";
import { TextInput, TextInputProps } from "./TextInput";
import { FieldValues } from "react-hook-form";
import { FormLabel } from "./TextInputLabel";

export type LabeledTextInputProps<T extends FieldValues> = {
  label: string;
} & TextInputProps<T>;

export const LabeledTextInput = <T extends FieldValues>({
  label,
  ...rest
}: LabeledTextInputProps<T>) => {
  return (
    <>
      <FormLabel htmlFor={rest.id}>{label}</FormLabel>
      <TextInput {...rest} />
    </>
  );
};
