import { useRouter } from "next/router";
import React, { FC } from "react";
import FormButton from "./FormButton";
import { SubmitFormButton } from "./SubmitFormButton";

export const Form: FC<
  {
    heading: string;
  } & React.InputHTMLAttributes<HTMLFormElement>
> = ({ heading, children, ...rest }) => {
  const router = useRouter();

  return (
    <div className="py-4">
      <h1 className="p-2 m-0 font-bold text-center text-white bg-red-400 shadow rounded-t-md ">
        {heading}
      </h1>
      <form
        className="px-8 py-4 mb-4 bg-white border-2 border-gray-300 rounded-b-lg shadow"
        {...rest}
      >
        {children}
        <SubmitFormButton>Submit</SubmitFormButton>
        <FormButton onClick={router.back}>Cancel</FormButton>
      </form>
    </div>
  );
};
