/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    theme: {
      extend: {
        colors: {
          primary: {
            default: "red-300",
            light: "#ff74b5",
            dark: "#b5004f",
          },
          secondary: {
            default: "#8a2be2",
            light: "#a57dff",
            dark: "#5f00b2",
          },
          background: {
            default: "#353a3b",
          },
          text: {
            default: "#333",
            light: "#555",
            dark: "#111",
          },
        },
      },
    },
    variants: {},
    plugins: [],
  },
  plugins: [],
};
