export const BASE_URL = "http://localhost:3000";
export const API_URL = BASE_URL + "/api";

// Resoure urls
export const PRODUCTS_URL = API_URL + "/products";
export const CATEGORIES_URL = API_URL + "/categories";
export const REVIEWS_URL = API_URL + "/reviews";
export const ORDERS_URL = API_URL + "/orders";
export const USERS_URL = API_URL + "/users";
// export const PHOTOS_URL = API_URL + "/photos";
