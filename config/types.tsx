import { Category, Order, Prisma, Product } from "@prisma/client";

export type ProductData = Omit<Product, "categories"> & {
  categoryIds: number[];
};

export type ProductWithCategories = Product & {
  categories: Category[];
};

export type ProductPostData = Omit<ProductData, "id">;

export type OrderPatchData = Omit<Order, "created" | "dollars" | "cents"> & {
  products: { productId: number; quantity: number }[];
};

export type OrderPostData = Omit<OrderPatchData, "id">;

export type UserPostData = {
  email: string;
  password: string;
  name: string;
  phone: string;
};

export type UserPatchData = {
  id: number;
} & UserPostData;

export type SortingType = "price" | "name" | "reviews";

export type ProductFilters = {
  text?: string;
  priceMin?: number;
  priceMax?: number;
  categories: number[];
  sortBy: SortingType;
  sortingDirection: Prisma.SortOrder;
  page: number;
};

export type ShoppingCartItem = {
  product: Product;
  quantity: number;
};
export type ShoppingCart = ShoppingCartItem[];

type hbnnm = {
  name: string;
  description: string | null;
  photo: string | null;
  inStock: number;
  isForSale: boolean;
  priceDollars: number;
  priceCents: number;
  categoryIds: number[];
};
