/*
  Warnings:

  - Added the required column `priceCents` to the `Product` table without a default value. This is not possible if the table is not empty.
  - Added the required column `priceDollars` to the `Product` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Product" ADD COLUMN     "priceCents" INTEGER NOT NULL,
ADD COLUMN     "priceDollars" INTEGER NOT NULL;
