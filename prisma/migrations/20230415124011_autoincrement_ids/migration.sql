-- AlterTable
CREATE SEQUENCE category_id_seq;
ALTER TABLE "Category" ALTER COLUMN "id" SET DEFAULT nextval('category_id_seq');
ALTER SEQUENCE category_id_seq OWNED BY "Category"."id";

-- AlterTable
CREATE SEQUENCE orderproduct_id_seq;
ALTER TABLE "OrderProduct" ALTER COLUMN "id" SET DEFAULT nextval('orderproduct_id_seq');
ALTER SEQUENCE orderproduct_id_seq OWNED BY "OrderProduct"."id";

-- AlterTable
CREATE SEQUENCE payment_id_seq;
ALTER TABLE "Payment" ALTER COLUMN "id" SET DEFAULT nextval('payment_id_seq');
ALTER SEQUENCE payment_id_seq OWNED BY "Payment"."id";

-- AlterTable
CREATE SEQUENCE productphoto_id_seq;
ALTER TABLE "ProductPhoto" ALTER COLUMN "id" SET DEFAULT nextval('productphoto_id_seq');
ALTER SEQUENCE productphoto_id_seq OWNED BY "ProductPhoto"."id";

-- AlterTable
CREATE SEQUENCE shipping_id_seq;
ALTER TABLE "Shipping" ALTER COLUMN "id" SET DEFAULT nextval('shipping_id_seq');
ALTER SEQUENCE shipping_id_seq OWNED BY "Shipping"."id";
