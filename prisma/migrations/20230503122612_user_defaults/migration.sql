-- AlterTable
ALTER TABLE "User" ALTER COLUMN "isSuperuser" SET DEFAULT false,
ALTER COLUMN "isBanned" SET DEFAULT false;
