Aplikaci je možné spustit na libovolné platformě s nainstalovaným kontejnerizačním softwarem Docker verze 20.10.21 a vyšší. Zadáním
příkazu

```
docker compose up
```

se aplikace spustí. První spuštění může aplikace na zařízení může trvat až desítky minut v zásvislosti na rychlosti připojení
k internetu a výkonu zařízení. Aplikace se totiž musí sestavit. K tomu jsou potřeba balíky uvedené v souboru Dockerfile, docker-compose.yml
a package.json.
Docker je stáhne automaticky, avšak vyžaduje k tomu připojení internetu.
Po spuštění je aplikace přístupná na adrese localhost:3000.

Port, na kterém aplikace běží, lze změnit v souboru docker-compose.yml přepsáním hodnoty "ports" ve službě "app". Přidáním možnosti "ports"
do služby "db" je případně možné odkrýt port, na kterém běží databáze, aby se bylo možné připojit na databázi běžící v kontejneru. Přístupové
údaje k ní jsou specifikovány v položce "enviroment" ve stejném souboru.

Konfiguraci aplikace je možné měnit souboru .env. Možnost DATABASE_URL v něm obsahuje řetězec pro připojení k databázi. BASE_URL je adresa,
na které se má aplikace spouštět - při změně portu v souboru docker-compose.yml je nutné změnit port i zde.
Položky SECRET a NEXTAUTH_SECRET jsou používány pro autentizaci. Tyto proměnné by měly být před nasazením aplikace změněny na náhodné řetězce
o délce alespoň 20 znaků. Pro autentizaci se také využívá proměnná SALT_ROUNDS, která udává, kolikrát se mají hesla uživatelů hashovat s
bezpečnostním prvkem salt. Proměnná PRODUCTS_PER_PAGE udává, kolik produktů se má zobrazit na jedné stránce aplikace.
