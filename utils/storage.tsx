export const getStorageItem = (key: string) => {
  if (typeof window === "undefined") return null;

  const item = localStorage.getItem(key);
  if (item) return JSON.parse(item);
  else return null;
};

export const setStorageItem = (key: string, value: any) => {
  if (typeof window !== "undefined") {
    localStorage.setItem(key, JSON.stringify(value));
  }
};
