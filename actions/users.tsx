import { Product } from "@prisma/client";
import { API_URL, USERS_URL } from "../config/paths";
import { UserPatchData, UserPostData } from "../config/types";

export const fetchOneUser = async (id: number) => {
  return fetch(`${USERS_URL}/${id}`, {
    method: "GET",
  }).then((res) => res.json());
};

export const fetchUsers = async () => {
  return fetch(`${USERS_URL}`, {
    method: "GET",
  }).then((res) => res.json());
};

export const createUser = async (user: UserPostData) => {
  return await fetch(`${USERS_URL}`, {
    method: "POST",
    body: JSON.stringify(user),
  });
};

export const updateUser = async (user: UserPatchData) => {
  return fetch(`${USERS_URL}/${user.id}`, {
    method: "PATCH",
    body: JSON.stringify(user),
  }).then((res) => res.json());
};

export const deleteUser = async (id: number) => {
  return fetch(`${USERS_URL}/${id}`, {
    method: "DELETE",
  }).then((res) => res.json());
};
