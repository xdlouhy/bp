import { API_URL, ORDERS_URL, PRODUCTS_URL, USERS_URL } from "../config/paths";
import { OrderPatchData, OrderPostData } from "../config/types";

export const createOrder = async (order: OrderPostData) => {
  return await fetch(`${ORDERS_URL}`, {
    method: "POST",
    body: JSON.stringify(order),
  });
};

export const updateOrder = async (order: OrderPatchData) => {
  return fetch(`${ORDERS_URL}/${order.id}`, {
    method: "PATCH",
    body: JSON.stringify(order),
  }).then((res) => res.json());
};

export const deleteOrder = async (id: number) => {
  return fetch(`${ORDERS_URL}/${id}`, {
    method: "DELETE",
  }).then((res) => res.json());
};

export const fetchOneOrder = async (id: number) => {
  return fetch(`${ORDERS_URL}/${id}`, {
    method: "GET",
  }).then((res) => res.json());
};

export const fetchProductOrders = async (productId: number) => {
  return fetch(`${PRODUCTS_URL}/${productId}/orders`, {
    method: "GET",
  }).then((res) => res.json());
};

export const fetchUserOrders = async (userId: number) => {
  return fetch(`${USERS_URL}/${userId}/orders`, {
    method: "GET",
  }).then((res) => res.json());
};
