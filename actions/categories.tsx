import { Category } from "@prisma/client";
import { CATEGORIES_URL } from "../config/paths";

export const fetchCategories = async () => {
  const res = await fetch(`${CATEGORIES_URL}`);
  const data = await res.json();
  return data;
};

export const deleteCategory = async (id: number) => {
  return fetch(`${CATEGORIES_URL}${id}`, {
    method: "DELETE",
  }).then((res) => res.json());
};

export const createCategory = async (category: Omit<Category, "id">) => {
  return await fetch(`${CATEGORIES_URL}`, {
    method: "POST",
    body: JSON.stringify(category),
  }).then((res) => res.json());
};
