import queryString from "query-string";
import { API_URL, PRODUCTS_URL } from "../config/paths";
import {
  ProductData,
  ProductPostData,
  ProductWithCategories,
} from "../config/types";
import { searchFilters } from "../signals";

export const fetchOneProduct = async (
  id: number
): Promise<ProductWithCategories> => {
  return fetch(`${PRODUCTS_URL}/${id}`, {
    method: "GET",
  }).then((res) => res.json());
};

export const fetchProducts = async (): Promise<ProductWithCategories[]> => {
  return fetch(
    `${PRODUCTS_URL}?` + queryString.stringify(searchFilters.value),
    {
      method: "GET",
    }
  ).then((res) => {
    return res.json();
  });
};

export const createProduct = async (product: ProductPostData) => {
  return await fetch(`${PRODUCTS_URL}`, {
    method: "POST",
    body: JSON.stringify(product),
  });
};

export const updateProduct = async (product: ProductData) => {
  return fetch(`${PRODUCTS_URL}/${product.id}`, {
    method: "PATCH",
    body: JSON.stringify(product),
  }).then((res) => res.json());
};

export const deleteProduct = async (id: number) => {
  return fetch(`${PRODUCTS_URL}/${id}`, {
    method: "DELETE",
  }).then((res) => res.json());
};

export const addProductToCategory = async (
  productId: number,
  categoryId: number
) => {
  return modifyProductCategory(productId, categoryId, "POST");
};

export const removeProductFromCategory = async (
  productId: number,
  categoryId: number
) => {
  return modifyProductCategory(productId, categoryId, "DELETE");
};

const modifyProductCategory = async (
  productId: number,
  categoryId: number,
  method: "POST" | "DELETE"
) => {
  return fetch(`${PRODUCTS_URL}/${productId}/categories`, {
    method: method,
    body: JSON.stringify({ categoryId: categoryId }),
  }).then((res) => res.json());
};
