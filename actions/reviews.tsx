import { Review } from "@prisma/client";
import { REVIEWS_URL } from "../config/paths";

export const createReview = async (review: Omit<Review, "id">) => {
  return await fetch(`${REVIEWS_URL}`, {
    method: "POST",
    body: JSON.stringify(review),
  });
};

export const deleteReview = async (id: number) => {
  return await fetch(`${REVIEWS_URL}`, {
    method: "DELETE",
    body: JSON.stringify(id),
  });
};

export const editReview = async (review: Partial<Review>) => {
  return await fetch(`${REVIEWS_URL}`, {
    method: "PATCH",
    body: JSON.stringify(review),
  });
};

export const fetchReviews = async (productId: number) => {
  const res = await fetch(`${REVIEWS_URL}?productId=${productId})`);
  const data = await res.json();
  return data;
};
