import { Product } from "@prisma/client";
import { useEffect, useState } from "react";
import { ShoppingCart } from "../config/types";
import { shoppingCart } from "../signals";
import { getStorageItem, setStorageItem } from "../utils/storage";

export const useCart = () => {
  if (getStorageItem("cart") === null) setStorageItem("cart", []);

  const [cart, setCart] = useState<ShoppingCart>([]);
  useEffect(() => {
    setCart(shoppingCart.value);
  }, [shoppingCart.value]);

  useEffect(() => {
    saveCart();
  }, [shoppingCart.value]);

  const addToCart = (product: Product, quantity: number) => {
    shoppingCart.value = [...shoppingCart.value, { product, quantity }];
  };

  const removeFromCart = (index: number) => {
    shoppingCart.value = shoppingCart.value.filter((_, i) => i !== index);
  };

  const updateCart = (index: number, quantity: number) => {
    shoppingCart.value = shoppingCart.value.map((item, i) =>
      i === index ? { ...item, quantity } : item
    );
  };

  const clearCart = () => {
    shoppingCart.value = [];
  };

  const saveCart = () => {
    setStorageItem("cart", shoppingCart.value);
  };

  return { addToCart, removeFromCart, updateCart, clearCart, cart };
};
