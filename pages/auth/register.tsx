import React from "react";
import { RegisterForm } from "../../components/froms/user/RegisterForm";

export const register = () => {
  return (
    <div className="flex flex-col justify-center my-4">
      <RegisterForm />
    </div>
  );
};

export default register;
