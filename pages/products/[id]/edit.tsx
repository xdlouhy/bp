import React from "react";
import { ProductForm } from "../../../components/froms/product/ProductForm";
import { fetchOneProduct } from "../../../actions/products";
import { useQuery } from "react-query";
import { useRouter } from "next/router";

export const edit = () => {
  const router = useRouter();
  const id = Number(router.query.id);
  const { data, isLoading } = useQuery({
    queryKey: ["product", id],
    queryFn: () => fetchOneProduct(id),
  });
  if (isLoading) {
    return <div>Loading...</div>;
  }
  return (
    <div className="flex flex-col justify-center py-4 mx-auto">
      <ProductForm product={data} />
    </div>
  );
};

export default edit;
