import { GetServerSidePropsContext } from "next";
import { useSession } from "next-auth/react";
import Image from "next/image";
import { useRouter } from "next/router";
import { FC } from "react";
import { useForm } from "react-hook-form";
import { useCart } from "../../../actions/cart";
import { deleteProduct, fetchOneProduct } from "../../../actions/products";
import { TextInput } from "../../../components/froms/TextInput";
import { PrimaryButton } from "../../../components/layout/Button/PrimaryButton";
import {
  getProductMainPhoto,
  isAdmin,
  isInCart,
} from "../../../components/product/utils";
import { ProductWithCategories } from "../../../config/types";

export const ProductDetail: FC<{ product: ProductWithCategories }> = ({
  product,
}) => {
  const { register, handleSubmit } = useForm<{ quantity: number }>({});
  const { data: session } = useSession();
  const router = useRouter();
  const { addToCart, cart } = useCart();

  return (
    <div className="flex flex-col items-center justify-center mx-16 my-16 gap">
      <div className="grid grid-cols-2 gap-8">
        <div className="flex items-center justify-center ">
          <div className="relative object-contain w-100px h-100px">
            <Image
              className="relative"
              src={getProductMainPhoto(product)}
              alt={product.name}
              width={400}
              height={400}
            />
          </div>
        </div>
        <div className="flex flex-col items-start justify-center">
          <h1 className="text-2xl font-bold">{product.name}</h1>
          <div className="pb-4">
            Categories:{" "}
            {product?.categories?.map((category) => category.name).join(", ")}
          </div>
          <p className="text-xl font-extralight ">
            Price: ${product.priceDollars}.{product.priceCents}
          </p>
          <hr className="h-px my-8 bg-gray-800 border-0 dark:bg-gray-700" />
          <p className="text-gray-600">{product.description}</p>
          <form
            onSubmit={handleSubmit((data) =>
              addToCart(product, data["quantity"])
            )}
            className="my-4"
          >
            <div className="flex flex-row gap-4 align-middle">
              <label htmlFor="quantity" className="text-xl font-extralight">
                Amount:{" "}
              </label>
              <TextInput
                register={register}
                type="number"
                id="quantity"
                defaultValue={1}
                min={1}
                className="w-16 h-8"
              />
            </div>
            {isInCart(cart, product.id) ? (
              <PrimaryButton
                preventDefault
                onClick={() => router.push("/cart")}
                className="mx-0 my-4"
              >
                In cart
              </PrimaryButton>
            ) : (
              <PrimaryButton className="mx-0 my-4" type="submit">
                {"Add to cart"}
              </PrimaryButton>
            )}
            {isAdmin(session) && (
              <PrimaryButton
                preventDefault
                className="my-4 ml-4"
                onClick={() => router.push(`${router.query.id}/edit`)}
              >
                {"Edit product"}
              </PrimaryButton>
            )}
            {isAdmin(session) && (
              <PrimaryButton
                preventDefault
                className="my-4 "
                onClick={async () => {
                  await deleteProduct(product.id);
                  router.push("/");
                }}
              >
                {"Delete product"}
              </PrimaryButton>
            )}
          </form>
        </div>
      </div>
    </div>
  );
};

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const id = Number(context.query.id);
  if (Number.isNaN(id)) {
    return null;
  }
  const product = await fetchOneProduct(id);

  return {
    props: {
      product,
    },
  };
};

export default ProductDetail;
