import { ProductForm } from "../../components/froms/product/ProductForm";

export const create = () => {
  return (
    <div className="flex flex-col justify-center py-4 mx-auto">
      <ProductForm />
    </div>
  );
};

export default create;
