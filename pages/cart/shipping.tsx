import { ShippingAddress } from "@prisma/client";
import { useSession } from "next-auth/react";
import { FC } from "react";
import { useForm } from "react-hook-form";
import { useCart } from "../../actions/cart";
import { createOrder } from "../../actions/orders";
import { Form } from "../../components/froms/Form";
import { LabeledTextInput } from "../../components/froms/LabeledTextInput";
import { OrderPostData, ShoppingCart } from "../../config/types";
import { useRouter } from "next/router";

const submitOrder = async (data: any, cart: ShoppingCart) => {
  const order: OrderPostData = {
    name: data.name as string,
    email: data.email as string,
    phone: data.phone as string,
    address: data.address as string,
    city: data.city as string,
    zip: data.zip as string,
    country: data.country as string,
    userId: Number(data.userId),

    products: cart.map((cartItem) => {
      return {
        productId: cartItem.product.id as number,
        quantity: Number(cartItem.quantity),
      };
    }),
  };

  console.log(order);
  await createOrder(order);
};

export const ShippingPage: FC<{ shippingAddress?: ShippingAddress }> = ({
  shippingAddress,
}) => {
  const { data: userData } = useSession();
  const { cart, clearCart } = useCart();
  const router = useRouter();
  const user = userData?.user;
  const { register, handleSubmit } = useForm({
    defaultValues: { ...shippingAddress, ...user, userId: user?.id },
  });
  return (
    <Form
      heading="Shipping"
      onSubmit={handleSubmit((data) => {
        submitOrder(data, cart);
        clearCart();
        router.push("/");
      })}
    >
      <LabeledTextInput
        id="name"
        register={register}
        options={{ validate: (value) => typeof value !== "undefined" }}
        label="Full name"
        className="w-full"
      />
      <LabeledTextInput id="email" register={register} label="Email" required />
      <LabeledTextInput id="phone" register={register} label="Phone" />
      <LabeledTextInput
        id="address"
        register={register}
        options={{ validate: (value) => typeof value !== "undefined" }}
        label="Address"
        className="w-full"
        required
      />
      <LabeledTextInput id="city" register={register} label="City" required />
      <LabeledTextInput
        id="zip"
        register={register}
        label="ZIP code"
        required
      />
      <LabeledTextInput
        id="country"
        register={register}
        label="Country"
        required
      />
    </Form>
  );
};

export default ShippingPage;
