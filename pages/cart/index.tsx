import Link from "next/link";
import { useCart } from "../../actions/cart";
import { CartItem } from "../../components/cart/CartItem";
import { PrimaryButton } from "../../components/layout/Button/PrimaryButton";
import { getCartTotal } from "../../components/product/utils";
import { ShoppingCart } from "../../config/types";

const isEmpty = (cart: ShoppingCart) => cart.length === 0;

export const Cart = () => {
  const { cart, clearCart } = useCart();
  const price = getCartTotal(cart);
  return (
    <div>
      <h1 className="py-4 text-2xl">
        Your shopping cart {isEmpty(cart) && "is empty"}
      </h1>

      <div className="flex flex-col">
        {cart.map((item, i) => (
          <CartItem key={`cartItem-${i}`} item={item} index={i} />
        ))}
      </div>
      <div className="flex flex-row justify-between py-4">
        <h2 className="text-xl font-bold">
          Total: {`$${price.total}.${price.totalCents}`}
        </h2>
        <div className="flex flex-row justify-end">
          {!isEmpty(cart) && (
            <>
              <PrimaryButton preventDefault onClick={() => clearCart()}>
                Clear cart
              </PrimaryButton>
              <Link href={"/cart/shipping"}>
                <PrimaryButton>Order</PrimaryButton>
              </Link>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default Cart;
