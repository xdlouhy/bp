import { useSession } from "next-auth/react";
import React, { useEffect } from "react";
import { useRouter } from "next/navigation";

export const Profile = () => {
  const { data: session } = useSession();
  const router = useRouter();

  useEffect(() => {
    if (!session || !session.user) router.push("/");
  }, [session]);

  return <div>Hello, {session?.user?.name}!</div>;
};
export default Profile;
