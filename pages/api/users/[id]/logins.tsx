import { NextApiRequest, NextApiResponse } from "next";
import {
  okOrNotFound,
  methodNotAllowed,
  isSuperuser,
  unauthorized,
} from "../../../../components/product/utils";
import prisma from "../../../../config/prisma";

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  if (!(await isSuperuser(req))) {
    return unauthorized(res);
  }

  const userId = Number(req.query.id);

  switch (req.method) {
    case "GET":
      return okOrNotFound(
        prisma.login.findMany({
          where: {
            userId: { equals: userId },
          },
        }),
        res
      );

    default:
      return methodNotAllowed(res);
  }
};

export default handler;
