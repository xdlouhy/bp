import { NextApiRequest, NextApiResponse } from "next";
import { getPrismaUserQuery } from "..";
import {
  isAllowed,
  isSuperuser,
  methodNotAllowed,
  okOrNotFound,
  unauthorized,
} from "../../../../components/product/utils";
import prisma from "../../../../config/prisma";
import { UserPatchData } from "../../../../config/types";

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const userId = Number(req.query.id);

  switch (req.method) {
    case "GET":
      if (!(await isAllowed(req, userId))) {
        return unauthorized(res);
      }
      return okOrNotFound(
        prisma.user.findUnique({
          where: {
            id: userId,
          },
        }),
        res
      );

    case "PATCH":
      if (!(await isAllowed(req, userId))) {
        return unauthorized(res);
      }

      const data: UserPatchData = JSON.parse(req.body);
      const user = await getPrismaUserQuery(data);

      const { id } = data;

      return okOrNotFound(
        prisma.user.update({ where: { id: id }, data: user }),
        res
      );

    case "DELETE":
      if (!(await isSuperuser(req))) {
        return unauthorized(res);
      }

      return okOrNotFound(prisma.user.delete({ where: { id: userId } }), res);

    default:
      return methodNotAllowed(res);
  }
};

export default handler;
