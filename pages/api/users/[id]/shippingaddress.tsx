import { NextApiRequest, NextApiResponse } from "next";
import {
  isAllowed,
  methodNotAllowed,
  okOrNotFound,
  unauthorized,
} from "../../../../components/product/utils";
import prisma from "../../../../config/prisma";
import { ShippingAddress } from "@prisma/client";

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const userId = Number(req.query.id);
  if (!isAllowed(req, userId)) {
    return unauthorized(res);
  }

  switch (req.method) {
    case "GET":
      return okOrNotFound(
        prisma.shippingAddress.findFirst({
          where: {
            userId: { equals: userId },
          },
        }),
        res
      );
    case "PATCH":
      if (!(await isAllowed(req, userId))) {
        return unauthorized(res);
      }
      const { body } = req;
      const address: Omit<ShippingAddress, "userId" | "id"> = JSON.parse(body);

      return okOrNotFound(
        prisma.shippingAddress.update({
          where: { userId: userId },
          data: address,
        }),
        res
      );

    default:
      return methodNotAllowed(res);
  }
};

export default handler;
