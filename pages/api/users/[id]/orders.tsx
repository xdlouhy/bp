import { NextApiRequest, NextApiResponse } from "next";
import {
  okOrNotFound,
  methodNotAllowed,
  isAllowed,
  unauthorized,
} from "../../../../components/product/utils";
import prisma from "../../../../config/prisma";

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const userId = Number(req.query.id);

  switch (req.method) {
    case "GET":
      if (!(await isAllowed(req, userId))) {
        return unauthorized(res);
      }

      return okOrNotFound(
        prisma.order.findMany({
          where: {
            userId: { equals: userId },
          },
        }),
        res
      );

    default:
      return methodNotAllowed(res);
  }
};

export default handler;
