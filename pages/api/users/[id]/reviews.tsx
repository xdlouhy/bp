import { Order, PrismaClient } from "@prisma/client";
import { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../../config/prisma";
import {
  isAllowed,
  methodNotAllowed,
  okOrNotFound,
  unauthorized,
} from "../../../../components/product/utils";

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const userId = Number(req.query.id);

  if (!(await isAllowed(req, userId))) {
    return unauthorized(res);
  }

  switch (req.method) {
    case "GET":
      return okOrNotFound(
        prisma.order.findMany({
          where: {
            userId: { equals: userId },
          },
        }),
        res
      );

    default:
      return methodNotAllowed(res);
  }
};

export default handler;
