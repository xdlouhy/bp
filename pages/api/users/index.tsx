import bcrypt from "bcrypt";
import { NextApiRequest, NextApiResponse } from "next";
import {
  okOrNotFound,
  methodNotAllowed,
  isSuperuser,
  unauthorized,
} from "../../../components/product/utils";
import prisma from "../../../config/prisma";
import { UserPostData } from "../../../config/types";

export const getPrismaUserQuery = async (data: UserPostData) => {
  const { password, ...rest } = data;
  const salt = await bcrypt.genSalt(Number(process.env.SALT_ROUNDS));
  const hashedPassword = await bcrypt.hash(password, salt);
  return {
    ...rest,
    passwordHash: hashedPassword,
  };
};

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case "GET":
      if (!isSuperuser(req)) {
        return unauthorized(res);
      }
      return okOrNotFound(prisma.user.findMany(), res);

    case "POST":
      const data: UserPostData = JSON.parse(req.body);
      if (await prisma.user.findUnique({ where: { email: data.email } })) {
        res.status(400).json({ message: "Email already registered." });
        return;
      }
      const user = await getPrismaUserQuery(data);

      return okOrNotFound(prisma.user.create({ data: user }), res);

    default:
      return methodNotAllowed(res);
  }
};

export default handler;
