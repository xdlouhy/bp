import { NextApiRequest, NextApiResponse } from "next";
import {
  isSuperuser,
  methodNotAllowed,
  okOrNotFound,
  unauthorized,
} from "../../../../components/product/utils";
import prisma from "../../../../config/prisma";
import { ProductPostData } from "../../../../config/types";

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const productId = Number(req.query.id);

  switch (req.method) {
    case "GET":
      return okOrNotFound(
        prisma.product.findUnique({
          where: {
            id: productId,
          },
          include: { categories: true },
        }),
        res
      );

    case "PATCH":
      if (!(await isSuperuser(req))) {
        return unauthorized(res);
      }
      const { body } = req;
      const data: ProductPostData = JSON.parse(body);
      console.log(data);
      const { categoryIds, ...rest } = data;
      await prisma.product.update({
        where: { id: productId },
        data: { categories: { set: [] } },
      });
      const product = {
        ...rest,
        categories: { connect: categoryIds.map((id) => ({ id })) },
      };
      return prisma.product
        .update({
          where: { id: productId },
          data: product,
        })
        .then((product) => res.status(200).json(product));
    case "DELETE":
      if (!(await isSuperuser(req))) {
        return unauthorized(res);
      }
      return okOrNotFound(
        prisma.product.delete({ where: { id: productId } }),
        res
      );

    default:
      return methodNotAllowed(res);
  }
};

export default handler;
