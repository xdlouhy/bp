import { NextApiRequest, NextApiResponse } from "next";
import {
  okOrNotFound,
  methodNotAllowed,
  isSuperuser,
  unauthorized,
} from "../../../../components/product/utils";
import prisma from "../../../../config/prisma";

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const productId = Number(req.query.id);

  switch (req.method) {
    case "GET":
      if (!(await isSuperuser(req))) {
        return unauthorized(res);
      }

      return okOrNotFound(
        prisma.order.findMany({
          where: {
            orderProducts: { some: { productId: productId } },
          },
        }),
        res
      );

    default:
      return methodNotAllowed(res);
  }
};

export default handler;
