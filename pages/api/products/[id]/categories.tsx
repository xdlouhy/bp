import { NextApiRequest, NextApiResponse } from "next";
import {
  isSuperuser,
  methodNotAllowed,
  okOrNotFound,
  unauthorized,
} from "../../../../components/product/utils";
import prisma from "../../../../config/prisma";

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const productId = Number(req.query.id);
  const { body } = req;
  const categoryId = JSON.parse(body).categoryId;

  switch (req.method) {
    case "GET":
      return okOrNotFound(
        prisma.product
          .findUnique({
            where: { id: productId },
            include: { categories: true },
          })
          .then((product) => product?.categories),
        res
      );

    case "POST":
      if (!isSuperuser(req)) {
        return unauthorized(res);
      }
      return okOrNotFound(
        prisma.product.update({
          where: { id: productId },
          data: { categories: { connect: { id: categoryId } } },
        }),
        res
      );

    case "DELETE":
      if (!isSuperuser(req)) {
        return unauthorized(res);
      }
      return okOrNotFound(
        prisma.product.update({
          where: { id: productId },
          data: { categories: { disconnect: { id: categoryId } } },
        }),
        res
      );

    default:
      return methodNotAllowed(res);
  }
};

export default handler;
