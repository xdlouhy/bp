import { PrismaClient } from "@prisma/client";
import { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../../../config/prisma";
import {
  okOrNotFound,
  methodNotAllowed,
} from "../../../../../components/product/utils";

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const productId = Number(req.query.id);

  switch (req.method) {
    case "GET":
      return okOrNotFound(
        prisma.review.findMany({
          where: {
            productId: { equals: productId },
          },
        }),
        res
      );
    case "POST":
    //TODO

    default:
      return methodNotAllowed(res);
  }
};

export default handler;
