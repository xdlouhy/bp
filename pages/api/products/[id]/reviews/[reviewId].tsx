import { NextApiRequest, NextApiResponse } from "next";
import {
  getResourceOwner,
  isAllowed,
  isSuperuser,
  methodNotAllowed,
  okOrNotFound,
  unauthorized,
} from "../../../../../components/product/utils";
import prisma from "../../../../../config/prisma";

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const reviewId = Number(req.query.reviewId);

  switch (req.method) {
    case "GET":
      return okOrNotFound(
        prisma.review.findUnique({
          where: {
            id: reviewId,
          },
        }),
        res
      );

    case "PATCH":
      const { body } = req;
      if (
        !(await isAllowed(req, await getResourceOwner(prisma.review, reviewId)))
      ) {
        return unauthorized(res);
      }
      return okOrNotFound(
        prisma.review.update({
          where: { id: reviewId },
          data: JSON.parse(body),
        }),
        res
      );

    case "DELETE":
      if (
        !(await isAllowed(req, await getResourceOwner(prisma.review, reviewId)))
      ) {
        return unauthorized(res);
      }

      return okOrNotFound(
        prisma.category.delete({ where: { id: reviewId } }),
        res
      );

    default:
      return methodNotAllowed(res);
  }
};

export default handler;
