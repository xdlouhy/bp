import { Prisma } from "@prisma/client";
import { NextApiRequest, NextApiResponse } from "next";
import {
  isSuperuser,
  methodNotAllowed,
  okOrNotFound,
  unauthorized,
} from "../../../components/product/utils";
import prisma from "../../../config/prisma";
import { ProductData, ProductFilters } from "../../../config/types";

const getSortingObject = (
  sortBy: string | undefined,
  sortingDirection: Prisma.SortOrder | undefined
): Prisma.Enumerable<Prisma.ProductOrderByWithRelationInput> | undefined => {
  if (typeof sortingDirection === "undefined") {
    sortingDirection = "asc";
  }
  switch (sortBy) {
    case "price":
      return [
        { priceDollars: sortingDirection },
        { priceCents: sortingDirection },
      ];
    case "reviews":
      return { reviews: { _count: sortingDirection } };
    case "name":
    default:
      return { name: sortingDirection };
  }
};

const getCategories = (
  categories: number[] | string | string[] | undefined
) => {
  if (typeof categories === "string") {
    console.log([Number(categories)]);
    return [Number(categories)];
  }
  if (Array.isArray(categories)) {
    console.log(categories);
    return categories.map((id) => Number(id));
  }
  return [];
};

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case "GET":
      const {
        text,
        priceMin,
        priceMax,
        categories,
        sortBy,
        sortingDirection,
        page,
      }: Partial<ProductFilters> = req.query;

      return prisma.product
        .findMany({
          include: { categories: true },
          where: {
            name: {
              contains: text as string,
              mode: "insensitive",
            },
            priceDollars: {
              ...(priceMin && { gte: Number(priceMin) }),
              ...(priceMax && { lte: Number(priceMax) }),
            },

            ...(categories && {
              categories: {
                some: {
                  id: { in: getCategories(categories) },
                },
              },
            }),
          },
          orderBy: getSortingObject(sortBy, sortingDirection),
          skip: page ? (page - 1) * Number(process.env.PRODUCTS_PER_PAGE) : 0,
          take: Number(process.env.PRODUCTS_PER_PAGE),
        })
        .then((products) => res.status(200).json(products));

    case "POST":
      if (!(await isSuperuser(req))) {
        return unauthorized(res);
      }

      const { body } = req;
      const data: ProductData = JSON.parse(body);
      const { categoryIds, ...rest } = data;
      const product = {
        categories: { connect: categoryIds.map((id) => ({ id })) },
        ...rest,
      };

      return okOrNotFound(prisma.product.create({ data: product }), res);

    default:
      return methodNotAllowed(res);
  }
};
export default handler;
