import { NextApiRequest, NextApiResponse } from "next";
import {
  isSuperuser,
  methodNotAllowed,
  okOrNotFound,
  unauthorized,
} from "../../../components/product/utils";
import prisma from "../../../config/prisma";

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case "GET":
      return okOrNotFound(prisma.category.findMany(), res);
    case "POST":
      if (!(await isSuperuser(req))) {
        return unauthorized(res);
      }
      const { body } = req;

      return okOrNotFound(
        prisma.category.create({
          data: JSON.parse(body),
        }),
        res
      );
    default:
      methodNotAllowed(res);
      return;
  }
};

export default handler;
