import { NextApiRequest, NextApiResponse } from "next";
import {
  isSuperuser,
  methodNotAllowed,
  okOrNotFound,
  unauthorized,
} from "../../../components/product/utils";
import prisma from "../../../config/prisma";

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const categoryId = Number(req.query.id);

  switch (req.method) {
    case "GET":
      return okOrNotFound(
        prisma.category.findUnique({
          where: {
            id: categoryId,
          },
        }),
        res
      );

    case "PATCH":
      const { body } = req;
      if (!(await isSuperuser(req))) {
        return unauthorized(res);
      }
      return okOrNotFound(
        prisma.category.update({
          where: { id: categoryId },
          data: JSON.parse(body),
        }),
        res
      );

    case "DELETE":
      if (!(await isSuperuser(req))) {
        return unauthorized(res);
      }

      return okOrNotFound(
        prisma.category.delete({ where: { id: categoryId } }),
        res
      );

    default:
      return methodNotAllowed(res);
  }
};

export default handler;
