import NextAuth from "next-auth/next";
import CredentialProviders from "next-auth/providers/credentials";
import bcrypt from "bcrypt";
import prisma from "../../../config/prisma";
import { User } from "@prisma/client";

export default NextAuth({
  secret: process.env.NEXTAUTH_SECRET,
  session: {
    strategy: "jwt",
  },
  providers: [
    CredentialProviders({
      name: "credentials",
      credentials: {
        email: {
          label: "Email",
          type: "email",
          placeholder: "user@example.com",
        },
        password: { label: "Password", type: "password" },
      },

      //@ts-ignore
      async authorize(credentials, req) {
        const user = await prisma.user.findUnique({
          where: { email: credentials?.email },
        });

        if (!user) return null;
        if (typeof credentials === "undefined") return null;

        if (await bcrypt.compare(credentials.password, user.passwordHash)) {
          return user;
        } else {
          return null;
        }
      },
    }),
  ],
  callbacks: {
    async session({ session, token, user }) {
      const dbUser = await prisma.user.findUnique({
        where: { email: session?.user.email },
      });

      if (!dbUser) return session;
      return {
        ...session,
        user: {
          ...session.user,
          id: Number(dbUser?.id),
          isSuperuser: dbUser.isSuperuser,
          phone: dbUser.phone,
        },
      };
    },
  },
});
