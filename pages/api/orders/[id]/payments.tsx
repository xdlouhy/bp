import { NextApiRequest, NextApiResponse } from "next";

import {
  getResourceOwner,
  isAllowed,
  isSuperuser,
  methodNotAllowed,
  okOrNotFound,
  unauthorized,
} from "../../../../components/product/utils";
import prisma from "../../../../config/prisma";

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const orderId = Number(req.query.id);
  switch (req.method) {
    case "GET":
      if (
        !(await isAllowed(req, await getResourceOwner(prisma.order, orderId)))
      ) {
        return unauthorized(res);
      }

      return okOrNotFound(
        prisma.payment.findMany({ where: { orderId: orderId } }),
        res
      );
    case "POST":
      if (!(await isSuperuser(req))) {
        return unauthorized(res);
      }
      const { body } = req;

      return okOrNotFound(
        prisma.payment.create({
          data: { ...JSON.parse(body), orderId: orderId },
        }),
        res
      );
    default:
      methodNotAllowed(res);
      return;
  }
};

export default handler;
