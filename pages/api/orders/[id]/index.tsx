import { NextApiRequest, NextApiResponse } from "next";
import { getPrismaOrderQuery } from "..";
import {
  getResource,
  getResourceOwner,
  isAllowed,
  methodNotAllowed,
  okOrNotFound,
  unauthorized,
} from "../../../../components/product/utils";
import prisma from "../../../../config/prisma";
import { OrderPatchData } from "../../../../config/types";
import { Order } from "@prisma/client";

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const orderId = Number(req.query.id);

  switch (req.method) {
    case "GET":
      return okOrNotFound(getResource<Order>(prisma.order, orderId), res);

    case "PATCH":
      if (!isAllowed(req, await getResourceOwner(prisma.order, orderId))) {
        return unauthorized(res);
      }

      const { body } = req;

      const data = JSON.parse(body);
      const prismaQuery = await getPrismaOrderQuery(data);

      const { id, products }: OrderPatchData = data;
      const productIds = products.map((product) => product.productId);

      okOrNotFound(
        prisma.order.update({
          where: { id: id },
          data: {
            ...prismaQuery,
            orderProducts: { connect: productIds.map((id) => ({ id })) },
          },
        }),
        res
      );
      return;

    case "DELETE":
      if (!isAllowed(req, await getResourceOwner(prisma.order, orderId))) {
        return unauthorized(res);
      }
      okOrNotFound(prisma.order.delete({ where: { id: orderId } }), res);
      return;

    default:
      methodNotAllowed(res);
  }
};

export default handler;
