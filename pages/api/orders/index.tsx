import { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../config/prisma";
import { OrderPostData } from "../../../config/types";
import {
  okOrNotFound,
  getCartTotal,
  getItemTotal,
  methodNotAllowed,
  zip,
  isAllowed,
  unauthorized,
  isSuperuser,
  getSenderId,
} from "../../../components/product/utils";
import { getServerSession } from "next-auth";

export const getPrismaOrderQuery = async (data: OrderPostData) => {
  const { products: cart, userId, ...rest }: OrderPostData = data;

  const quantities = cart.map(({ quantity }) => quantity);
  const { total, totalCents } = await prisma.product
    .findMany({
      where: {
        id: { in: cart.map(({ productId }) => productId) },
      },
    })

    .then((products) =>
      getCartTotal(
        products.map((product, index) => {
          return { product: product, quantity: quantities[index] };
        })
      )
    );
  return {
    dollars: total,
    cents: totalCents,
    ...rest,
  };
};

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case "GET":
      if (!(await isSuperuser(req))) {
        return unauthorized(res);
      }
      return okOrNotFound(prisma.order.findMany(), res);

    case "POST":
      const { body } = req;

      const data: OrderPostData = JSON.parse(body);
      const prismaObject = await getPrismaOrderQuery(data);
      const userId = await getSenderId(req);

      const { products } = data;

      return prisma.order
        .create({
          data: {
            ...prismaObject,
            orderProducts: { createMany: { data: products } },
            userId: userId ? userId : null,
          },
        })
        .then((order) => {
          res.status(200).json(order);
        });

    default:
      return methodNotAllowed(res);
  }
};

export default handler;
