import Head from "next/head";
import { useQuery } from "react-query";
import { fetchProducts } from "../actions/products";
import { ProductsContainer } from "../components/layout/ProductsContainer/ProductsContainer";
import { ProductTile } from "../components/product/ProductTile/ProductTile";
import { ProductWithCategories } from "../config/types";
import { searchFilters } from "../signals";
import { SearchFiltersForm } from "../components/froms/search/SearchFiltersForm";

export default function Home() {
  const {
    isLoading: productsIsLoading,
    data: productData,
    isError: productsIsError,
  } = useQuery({
    queryKey: ["products", searchFilters.value],
    queryFn: () => fetchProducts(),
  });

  return (
    <>
      <Head>
        <title>Testshop</title>
      </Head>
      <main>
        {productsIsError && <p>error</p>}
        {productsIsLoading && <p>loading</p>}
        <SearchFiltersForm />
        <ProductsContainer>
          {!productsIsLoading &&
            productData?.map &&
            productData?.map((product: ProductWithCategories) => (
              <div key={product.id}>
                <ProductTile product={product}></ProductTile>
              </div>
            ))}
        </ProductsContainer>
      </main>
    </>
  );
}

// export const getStaticProps = () => {
//   return {
//     props: {
//       categories: fetchCategories(),
//     },
//   };
// };
