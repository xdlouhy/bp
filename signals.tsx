import { Signal, signal } from "@preact/signals-react";
import { ProductFilters, ShoppingCart } from "./config/types";
import { getStorageItem } from "./utils/storage";
import { Category } from "@prisma/client";

export const searchFilters: Signal<ProductFilters> = signal({
  sortBy: "name",
  sortingDirection: "asc",
  page: 1,
  categories: [],
});

export const shoppingCart: Signal<ShoppingCart> = signal(
  getStorageItem("cart") || []
);
export const categories: Signal<Category[]> = signal([]);
